console.log('No. 1 - Animal Class')
console.log('Release 0')
class Animal {
    constructor(name) {
        this._name = name;
        this._legs = 4;
        this._cold_blooded = false;
    }
    get name() {
        return this._name;
    }
    get legs() {
        return this._legs;
    }
    get cold_blooded() {
        return this._cold_blooded;
    }
}
var sheep = new Animal("shaun");
console.log(sheep.name)
console.log(sheep.legs)
console.log(sheep.cold_blooded)
console.log('')
 
console.log('Release 0')  
class Ape extends Animal {
    constructor(name) {
        super(name)
        this._sound = "Auooo"
    }
    get legs() {
        return this._legs - 2;
    }
    yell() {
        console.log(this._sound)
    }
}
var sungokong = new Ape("kera Sakti")
console.log(sungokong.name)
console.log(sungokong.legs)
sungokong.yell()
console.log('')

class Frog extends Animal {
    constructor(name) {
        super(name)
        this._sound = 'Hop Hop'
    }
    get legs() {
        return this._legs;
    }
    jump() {
        console.log(this._sound)
    }
}
var kodok = new Frog("buduk")
console.log(kodok.name)
console.log(kodok.legs)
kodok.jump()
console.log('')

console.log('No.2 - Function to Class')
class Clock {
    constructor({ template }) {
        this.template = template;
    }
    render() {
      let date = new Date();
      let hours = date.getHours();
      if (hours < 10) hours = '0' + hours;
      let mins = date.getMinutes();
      if (mins < 10) mins = '0' + mins;
      let secs = date.getSeconds();
      if (secs < 10) secs = '0' + secs;
      let output = this.template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);
      console.log(output);
    }
    stop() {
      clearInterval(this.timer);
    }
    start() {
      this.render();
      this.timer = setInterval(() => this.render(), 1000);
    }
}
var clock = new Clock({template: 'h:m:s'});
clock.start();