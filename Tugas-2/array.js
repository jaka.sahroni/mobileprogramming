console.log('2. Array')
console.log()

console.log('No. 1 (Range)')
function range(startNum, finishNum) {
    var array = []
    if (startNum < finishNum) {
        for (var i = startNum; i <= finishNum; i++) {
            array.push(i)
        }
    } else if (startNum > finishNum) {
        for (var i = startNum; i >= finishNum; i--) {
            array.push(i)
        }
    } else {
        array.push(-1)    
    }
    return array
}
console.log(range(1, 10))
console.log(range(1))
console.log(range(11, 18))
console.log(range(54, 50))
console.log(range())
console.log()

console.log('No. 2 (Range with Step)')
function rangeWithStep(startNum, finishNum, step) {
    var array = []
    if (startNum < finishNum) {
        for (var i = startNum; i <= finishNum; i+=step) {
            array.push(i)
        }
    } else if (startNum > finishNum) {
        for (var i = startNum; i >= finishNum; i-=step) {
            array.push(i)
        }
    } else {
        array.push(-1)    
    }
    return array
}
console.log(rangeWithStep(1, 10, 2))
console.log(rangeWithStep(11, 23, 3))
console.log(rangeWithStep(5, 2, 1))
console.log(rangeWithStep(29, 2, 4))
console.log()

console.log('No. 3 (Sum of Range)')
function sum(start, finish, step) {
    var array = [];
    if (start == null && finish == null && step == null) {
        array.push(0);
        var sum = array[0];
    }
    else if (start < finish && step == null) {
        for (var i = start; i <= finish; i++) {
            array.push(i);
        }
        var sum = array.reduce((a, b) => { return a + b; }, 0)
    }
    else if (start > finish && step == null) {
        for (var j = start; j >= finish; j--) {
            array.push(j);
        }
        var sum = array.reduce((a, b) => { return a + b; }, 0)
    }
    else if (start < finish) {
        for (var k = start; k <= finish; k += step) {
            array.push(k);
        }
        var sum = array.reduce((a, b) => { return a + b; }, 0)
    }
    else if (start > finish) {
        for (var l = start; l >= finish; l -= step) {
            array.push(l);
        }
        var sum = array.reduce((a, b) => { return a + b; }, 0)
    }
    else if (finish == null && step == null) {
        array.push(1);
        var sum = array.reduce((a, b) => { return a + b; }, 0)
    }
    return sum;
}
console.log(sum(1, 10))
console.log(sum(5, 50, 2))
console.log(sum(15, 10))
console.log(sum(20, 10, 2))
console.log(sum(1))
console.log(sum())
console.log("");

console.log('No. 4 (Array Multidimensi)')
function dataHandling(numbers) {
     var input = [
        ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
        ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
        ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
        ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
    ]
     for (var x = numbers; x < 4; x++) {   
         console.log('Nomor ID: ' + input[x][0])
         console.log('Nama Lengkap: ' + input[x][1])
         console.log('TTL: ' + input[x][2] + ' ' + input[x][3])
         console.log('Hobi: ' + input[x][4])
         console.log()
        }
}
console.log(dataHandling(0))
console.log()

console.log('No. 5 (Balik Kata)')
function balikKata(str) {
    var currentString = str;
    var newString = '';
    for (var i = str.length - 1; i >= 0; i--) {
        newString = newString + currentString[i];
    }
    return newString;
}
console.log(balikKata("Kasur Rusak"))
console.log(balikKata("Informatika"))
console.log(balikKata("Haji Ijah"))
console.log(balikKata("racecar"))
console.log(balikKata("I am Humanikers"))
console.log()

console.log('No. 6 (Metode Array)')
function dataHandling2(biodata) {
    var biodata = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"]
    biodata.splice(1, 1, "Roman Alamsyah Elsharawy")
    biodata.splice(2, 1, "Provinsi Bandar Lampung")
    biodata.splice(4, 0, "Pria")
    biodata.splice(5, 4, "SMA Internasional Metro")
    console.log(biodata)
    
    var TTl = biodata[3]
    var tanggal = TTl.split("/")
    
    var bulan = tanggal[1]
    switch(bulan) {
      case '01': { bulan = ' Januari '; break; }
      case '02': { bulan = ' Februari '; break; }
      case '03': { bulan = ' Maret '; break; }
      case '04': { bulan = ' April '; break; }
      case '05': { bulan = ' Mei '; break; }
      case '06': { bulan = ' Juni '; break; }
      case '07': { bulan = ' Juli '; break; }
      case '08': { bulan = ' Agustus '; break; }
      case '09': { bulan = ' September '; break; }
      case '10': { bulan = ' Oktober '; break; }
      case '11': { bulan = ' November '; break; }
      case '12': { bulan = ' Desember '; break; }
    }
    console.log(bulan)
    
    var numbers = tanggal
    numbers.sort(function (value1, value2) { return value2 - value1 } ) ;
    console.log(numbers)
    
    var title = tanggal
    title.sort(function (value1, value2) { return value1 - value2 } ) ;
    var slug = title.join("-")
    console.log(slug)
    
    var nama = biodata[1]
    var panjang = nama.split("")
    var huruf = panjang
    var irisan1 = huruf.slice(0,14)
    var panggilan = irisan1.join("")
    console.log(panggilan)
    }
    console.log(dataHandling2())
