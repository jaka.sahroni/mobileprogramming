import React, { Component } from 'react';
import { View } from 'react-native';
import MenuAwal from './src/MenuAwal';
import FormLogin from './src/FormLogin';

export default class App extends Component {
  
  render() {
    return (
      <View>
        {/* <MenuAwal /> */}
        <FormLogin />
      </View>
    )
  }
};

