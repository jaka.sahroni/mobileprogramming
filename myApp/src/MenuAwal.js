import React from "react";
import { View, StyleSheet, Image, StatusBar,TouchableOpacity, Text } from "react-native";

const styles = StyleSheet.create({
  container: {
    paddingTop: 100,
    backgroundColor: '#22065E',
    height: 640,
  },
  tinyLogo: {
    margin: 50,
  },
  Menu: {
    flexDirection: 'row',
    marginTop: 200,
    marginLeft: 20,
    marginRight: 20,
    justifyContent: 'space-between',
  },
  button1: {
    borderRadius: 30,
    alignItems: "center",
    justifyContent: 'center',
    backgroundColor: "#fff",
    width: 150,
    height: 50,
  },
  button2: {
    borderRadius: 30,
    alignItems: "center",
    justifyContent: 'center',
    backgroundColor: "#8B0000",
    width: 150,
    height: 50,
  },
  title1: {
      color: '#000000',
      fontSize: 23,
      fontWeight: 'bold',
  },
  title2: {
    color: '#fff',
    fontSize: 23,
    fontWeight: 'bold',
},
});

  const MenuAwal= () => {
      return (
        <View style={styles.container}>
          <StatusBar
          backgroundColor="#131401"
          barStyle="light-content"
          />
          <Image
            style={styles.tinyLogo}
            source={require('./../assets/Logoapps.png')}
          />
         <View style={styles.Menu}>
          <TouchableOpacity
          style={styles.button1}
          >
            <Text style={styles.title1}>Masuk</Text>
          </TouchableOpacity>
             
          <TouchableOpacity
          style={styles.button2}
          >
            <Text style={styles.title2}>Daftar</Text>
          </TouchableOpacity>
          </View>
        </View>
      );
  };
  
  export default MenuAwal;