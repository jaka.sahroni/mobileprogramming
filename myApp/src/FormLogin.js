import React from "react";
import { View, StyleSheet, Image, StatusBar,TouchableOpacity, Text, TextInput } from "react-native";

const styles = StyleSheet.create({
  container: {
    paddingTop: 20,
    backgroundColor: '#22065E',
    height: 640,
  },
  signin :{
      color: '#fff',
      fontSize: 36,
      paddingLeft: 10,
  },
  Login: {
    margin: 20,
    marginBottom: 10,
    borderWidth: 5,
    backgroundColor: '#22065E',
    borderColor: '#E5EA10',
    borderRadius: 30,
    width: 325,
    height: 400,
  },
  tinyLogo: {
    marginTop: 20,
    marginLeft: 40,
    marginRight: 50,
    width: 245,
    height: 79,
  },
  Menu: {
    marginTop: 20,
    justifyContent: 'space-between',
  },
  Text: {
    marginTop: 7,
    marginLeft: 12,
    color: '#fff'
  },
  input: {
    height: 40,
    marginTop: 5,
    marginLeft: 12,
    paddingLeft: 12,
    marginRight: 12,
    borderWidth: 1,
    borderColor: '#fff',
    backgroundColor: '#fff',
  },
  button1: {
    marginTop: 20,
    marginLeft: 7,
    marginRight: 7,
    borderRadius: 30,
    alignItems: "center",
    justifyContent: 'center',
    backgroundColor: "#fff",
    width: 300,
    height: 50,
  },
  title1: {
      color: '#000000',
      fontSize: 23,
      fontWeight: 'bold',
  },
  Lupa: {
    color: '#3F0DAB',
    marginTop: 10,
    marginLeft: 110,
    marginRight: 100,
    borderRadius: 30,
  },
  signup: {
    flexDirection: 'row',
    marginLeft: 70,
  },
  daftar1: {
    color: '#fff',
  },
  daftar2: {
    color: '#3F0DAB',
    marginLeft: 2,
  },
});

  const Login= () => {
      return (
        <View style={styles.container}>
          <StatusBar
          backgroundColor="#131401"
          barStyle="light-content"
          />
          <Text style={styles.signin}>Sign In</Text>
          <View style={styles.Login}>
          <Image
            style={styles.tinyLogo}
            source={require('./../assets/LogoappsLogin.png')}
          />
         <View style={styles.Menu}>
             <Text style={styles.Text}>Email/Username</Text>
             <TextInput
             style={styles.input}
             placeholder="Masukkan Email/Username"
             />
             <Text style={styles.Text}>Password</Text>
             <TextInput
             style={styles.input}
             placeholder="Masukkan Password"
             keyboardType="numeric"
             />
          <TouchableOpacity
          style={styles.button1}
          >
            <Text style={styles.title1}>Masuk</Text>
          </TouchableOpacity>
          </View>
          <View>
              <Text style={styles.Lupa} >Lupa Password</Text>
          </View>
          </View>
          <View style={styles.signup}>
              <Text style={styles.daftar1} >Belum memiliki Akun MyBiodata?</Text>
              <Text style={styles.daftar2} >Daftar</Text>
          </View>
        </View>
      );
  };
  
  export default Login;